import {Component} from '@angular/core';
import {FormBuilder, FormArray} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  user = {
    fruits: [
      {name: 'Banana', selected: true},
      {name: 'Mango', selected: true},
      {name: 'Apple', selected: true},
      {name: 'Cherry', selected: true},
      {name: 'Coconut', selected: true},
      {name: 'Durian', selected: true},
      {name: 'Dragonfruit', selected: true},
      {name: 'Longan', selected: true},
      {name: 'Watermelon', selected: true},
      {name: 'Orange', selected: true}
    ]
  };
  form;

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      fruits: this.selectFruits()
    });
  }

  get fruits(): FormArray {
    return this.form.get('fruits') as FormArray;
  }


  selectFruits() {
    const arr = this.user.fruits.map(s => {
      return this.fb.control(s.selected);
    });
    return this.fb.array(arr);
  }
}
